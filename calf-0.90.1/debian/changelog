calf (0.90.1-1) unstable; urgency=medium

  * version 0.90.1 upstream
  * --enable-sse for x86 only to fix building on other architectures

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Fri, 29 Jun 2018 17:35:00 +0300
 
calf (0.90.0-2) unstable; urgency=medium

  * Rebuild package from Deepin
  * Change version 0.0.90 to 0.90.0 as in upstream (https://github.com/calf-studio-gear/calf/releases)

 -- Mikhail Novosyolov <mikhailnov@dumalogiya.ru>  Fri, 29 Jun 2018 17:35:00 +0300
 
calf (0.0.90-1) unstable; urgency=medium

  * update

 -- Deepin Packages Builder <packages@deepin.com>  Thu, 01 Mar 2018 16:10:21 +0800

calf (0.0.60-4) unstable; urgency=medium

  [ Sebastian Ramacher ]
  * Team upload.

  [ Johannes Brandstätter ]
  * Fix compilation errors when using GCC 6.

 -- Johannes Brandstätter <jbrandst@2ds.eu>  Sat, 24 Sep 2016 14:31:59 +0200

calf (0.0.60-3) unstable; urgency=medium

  * Update watch file:
    + Modernize to use format 4.
    + Mention gbp in usage hint comment.
    + Mangle upstrem prerelease versions.
  * Add patch cherry-picked upstream to fix comboboxes in Ardour4.
  * Declare compliance with Debian Policy 3.9.8.
  * Modernize copyright-check CDBS hints.
    + Strop include license-check script (part of cdbs now).
    + Tighten build-dependency on debhelper.
    + Build-depend on libipc-system-simple-perl.
    + Build-depend on libfont-ttf-perl (not lcdf-typetools).

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 18 Jun 2016 11:50:24 +0200

calf (0.0.60-2) unstable; urgency=medium

  * Extract metadata from images before copyright check.
    Build-depend on libimage-exiftool-perl libregexp-assemble-perl
    libipc-system-simple-perl lcdf-typetools.
  * Update Vcs-Git and Vcs-Browser URLs.
  * Update copyright info:
    + Fix extend coverage for main upstream authors
    + Fix bump packaging license to GPL-3+.
    + Fix update Files sections for autotools scripts and LV2 headers.
    + Fix add Files sections for jQuery-related code, and License
      sections ISC Expat.
    + Merge same-license Files sections.
    + Extend coverage of packaging.
    + Use License short-names FSFUL (and derivations) and X11.
    + Use License-Grant and License-Reference fields.
      Thanks to Ben Finney.
  * Unfuzz patch 1002, and tidy its DEP-3 header.
  * Revive documentation (gutted in 0.0.60-1):
    + Add patch 1001 to resolve paths from autoconf.
    + Add patch 2001 to use jQuery.colorbox (not jQuery.prettyPhoto).
    + Adjust location of documentation.
    + Symlink separately packaged Javascript.
    + (Build-)depend on libjs-jquery libjs-jquery-colorbox.
    + Stop strip documentation (now DFSG compliant).
    + Put aside and regenerate autotools during build.
    + Stop include autoreconf.mk.
    + Stop build-depend on dh-autoreconf.
    + Tighten build-dependency on cdbs: Needed for routines to put
      upstream autotools files aside during build.
  * Update Homepage.
  * Update source URLs.
  * Declare compliance with Debian Policy 3.9.7.
  * Add lintian override regarding license in License-Reference field.
    See bug#786450.
  * Bump debhelper compatibility level to 9.
  * Add lintian override regarding debhelper 9.
  * Fix rename TODO file got get included in binary package, and drop
    done items.
  * Modernize git-buildpackage config: Avoid git- prefix.
  * Override lintian for jquery.prettyPhoto.js missing source: False
    positive.
  * Enable SSE flags for AMD64.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 07 Mar 2016 11:23:34 +0100

calf (0.0.60-1) unstable; urgency=low

  [ Aurelien Martin ]
  * Imported Upstream version 0.0.60 (Closes: #783413)

  [ Adrian Knoth ]
  * Drop /usr/share/doc/calf/ to work around lintian errors

 -- Adrian Knoth <adi@drcomp.erfurt.thur.de>  Thu, 07 May 2015 14:42:11 +0200

calf (0.0.19+git20140915+5de5da28-1) unstable; urgency=medium

  * Imported Upstream version 0.0.19+git20140915+5de5da28 (Closes: #759833)

 -- Adrian Knoth <adi@drcomp.erfurt.thur.de>  Mon, 15 Sep 2014 22:19:42 +0200

calf (0.0.19+git20140527+e79ddef54-1) unstable; urgency=medium

  [ Adrian Knoth ]
  * Imported Upstream version 0.0.19+git20140527+e79ddef54
  * Bump standards version
  * Fix typo in package description (Closes: #745285)
  * Regenerate control

 -- Adrian Knoth <adi@drcomp.erfurt.thur.de>  Tue, 27 May 2014 20:48:47 +0000

calf (0.0.19+git20131202-1) unstable; urgency=low

  * Imported Upstream version 0.0.19+git20131202

 -- Adrian Knoth <adi@drcomp.erfurt.thur.de>  Mon, 02 Dec 2013 22:23:51 +0100

calf (0.0.19-2) unstable; urgency=low

  [ Jonas Smedegaard ]
  * Update README.source to emphasize control.in file as *not* a
    show-stopper for contributions, referring to wiki page for details.
  * Add git URL as alternate source.
  * Build-depend on dh-buildinfo: Backports- and multiarch-friendly
    nowadays.
  * Do copyright-check by default: Backports-friendly nowadays.
    Build-depend on devscripts.
  * Simplify moving aside upstream cruft.
    Tighten build-dependency on cdbs.
    Fix use pseudo-comment sections in copyright file to obey silly
    restrictions of copyright format 1.0.
  * Bump packaging license to GPL-3+, and extend coverage to include
    current year.
  * Have git-buildpackage ignore upstream .gitignore files.
  * Enable build of experimental plugins.
  * Rewrite short and long descriptions, based on About page of recently
    added documentation.
  * Suggest ladish, and add hint in long description on when it is
    useful.
  * Fix build-depend explicitly on libcairo2-dev, and fix recommend
    gtk2-engines-pixbuf: Both needed since 0.0.19.
  * Fix stop build-depend on ladspa-sdk or dssi-dev: Unused since
    0.0.19.
  * Drop obsolete and unused patches.
  * Simplify patch 1002 to not include autogenerated file.

  [ Adrian Knoth ]
  * Bump standards version
  * Drop obsolete DMUA flag
  * Put aside upstream-shipped configure
  * Generate configure during build process
  * Build-depend on libtool (Closes: #702613)

 -- Adrian Knoth <adi@drcomp.erfurt.thur.de>  Thu, 08 Aug 2013 18:08:47 +0200

calf (0.0.19-1) experimental; urgency=low

  * New upstream release.
  * Depends on libfftw3-dev and libfluidsynth-dev.
  * Remove patches which doesn't seem to be useful anymore.
  * Rewrite patch to use lv2-dev rather than dummy lv2core.
  * Empty the dependency_libs field in .la file.

 -- Tiago Bortoletto Vaz <tiago@debian.org>  Mon, 19 Nov 2012 18:50:19 -0500

calf (0.0.18.6-5) unstable; urgency=low

  * Team upload.
  * Migrate from lv2core to lv2-dev.

 -- Alessio Treglia <alessio@debian.org>  Wed, 16 May 2012 22:17:38 +0200

calf (0.0.18.6-4) unstable; urgency=low

  [ Alessio Treglia ]
  * Add patch 0001 to migrate from libglade2 to gtkBuilder.
    Stop build-depending on libglade2-dev.

  [ Jonas Smedegaard ]
  * Bump debhelper compat level to 7.
  * Bump standards-version to 3.9.3.
  * Git-ignore .pc quilt subdir.
  * Use linux-any wildcard for libasound dependency.
    Stop build-depending on devscripts.
  * Relax to build-depend unversioned on libgtk2.0-dev, debhelper and
    libjack-dev: Required versions satisfied even in oldstable.
  * Extend my copyright for Debian packaging.
  * Rewrite copyright file using format 1.0.
  * Add patch 1001 to fix FTBFS with gcc 4.7 by including <unistd.h>.
    Closes: bug#667126. Thanks to Cyril Brulebois.

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 05 Apr 2012 12:04:07 +0200

calf (0.0.18.6-3) unstable; urgency=low

  * Team upload.

  [ Jonas Smedegaard ]
  * Adjust copyright file:
    + Extend some years.
    + Fix mention licensing exception in Files stanzas.
    + Fix use Expat as licensing short-name.
  * Suppress optional CDBS-resolved build-dependencies.

  [ Alessio Treglia ]
  * Drop LASH support.
  * Bump Standards.

 -- Alessio Treglia <alessio@debian.org>  Fri, 20 May 2011 08:49:06 +0200

calf (0.0.18.6-2) unstable; urgency=low

  [ Jonas Smedegaard ]
  * Tidy copyright file: Adjust a few stanzas and declare it formatted
    according to draft DEP5 rev135.
  * Fix add a missed owner to copyright file (no new licenses).
  * Drop local CDBS snippets and locally declared DEB_MAINTAINER_MODE:
    All part of main CDBS now.
  * Refer to FSF website (not postal address) in rules file header.
  * Fix ALSA build-dependency auto-resolving (CDBS no longer horribly
    executes control.in file as a shell script).
  * Improve watch file: Add usage comment; relax pattern to non-digit
    versions and non-gzip tarball compression.
  * Fix DEP5 synatx of copyright file: append ./ in all files sections.
  * Improve copyright file: Refer to FSF website (not postal address),
    and put reference below Debian-specific comment to hint that it is
    not verbatim copied.

  [ Alessio Treglia ]
  * debian/control: Add Provides: lv2-plugin field.

  [ Adrian Knoth ]
  * Imported Upstream version 0.0.18.6
  * Bump standards version

 -- Adrian Knoth <adi@drcomp.erfurt.thur.de>  Tue, 24 Aug 2010 17:28:19 +0200

calf (0.0.18.5-1) unstable; urgency=low

  * Initial release (Closes: #522151)

 -- Jonas Smedegaard <dr@jones.dk>  Thu, 11 Feb 2010 21:27:47 +0100
