Source: calf
Section: sound
Priority: extra
Maintainer: Debian Multimedia Maintainers <pkg-multimedia-maintainers@lists.alioth.debian.org>
Uploaders: Adrian Knoth <adi@drcomp.erfurt.thur.de>,
 Jonas Smedegaard <dr@jones.dk>,
 Tiago Bortoletto Vaz <tiago@debian.org>
Build-Depends: cdbs (>= 0.4.131~),
 autotools-dev,
 devscripts,
 libregexp-assemble-perl,
 libimage-exiftool-perl,
 libfont-ttf-perl,
 automake,
 autoconf,
 debhelper,
 dh-buildinfo,
 libglib2.0-dev,
 libgtk2.0-dev,
 libreadline-dev,
 libexpat1-dev,
 libjack-dev,
 libfftw3-dev,
 libfluidsynth-dev,
 libcairo2-dev,
 libasound2-dev [linux-any],
 lv2-dev,
 libtool,
 libjs-jquery,
 libjs-jquery-colorbox
Standards-Version: 3.9.8
Homepage: http://calf-studio-gear.org/
Vcs-Git: https://anonscm.debian.org/git/pkg-multimedia/calf.git
Vcs-Browser: https://anonscm.debian.org/cgit/pkg-multimedia/calf.git

Package: calf-plugins
Architecture: any
Depends: ${cdbs:Depends}, ${shlibs:Depends}, ${misc:Depends}
Recommends: gtk2-engines-pixbuf
Suggests: ladish
Provides: lv2-plugin
Description: Calf Studiogear - audio effects and sound generators
 The Calf plugins include all todays frequently used studio effects
 along with some feature-rich sound processors.
 .
 Calf Studiogear is designed to run under the most flexible plugin
 standard in Linux, namely LV2. So you're able to insert them in lots of
 plugin-capable audio software like Ardour, Rosegarden and others.
 .
 The Calf Rack offers you all Calf effects and synthesizers in an
 easy-to-use studio rack. Wire the effects through Jack to build your own
 studio environment and trigger the sound processors live or through a
 MIDI sequencer or tracker.
 .
 Calf Rack supports LADISH level 1.
